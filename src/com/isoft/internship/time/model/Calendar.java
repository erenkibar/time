package com.isoft.internship.time.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Calendar {
    private List<Event> eventList = new ArrayList<>();

    public Calendar(){};

    public void addEvent(String name, String date){
        Event event = new Event(name, date);
        eventList.add(event);
    }
    public void removeEvent(String name){
        eventList.removeIf(event -> event.getEventName() == name);
    }
    public void show(){
        for (Event event : eventList)
            System.out.println(event.show());
    }

    public boolean isValidDate(String date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-mm-yyyy");
        simpleDateFormat.setLenient(false);
        try {
            simpleDateFormat.parse(date);

        }catch(ParseException e){
            System.out.println("Date is not valid!");
            return false;
        }
        return true;
    }

}
