package com.isoft.internship.time.utility;

import com.isoft.internship.time.model.Calendar;
import com.isoft.internship.time.model.Event;

import java.text.SimpleDateFormat;
import java.util.*;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    private Calendar calendar = new Calendar();

    public Menu(){};

    public void mainMenu(){
        System.out.println("\n1. Add an event.");
        System.out.println("2. See all events");
        System.out.println("3. Exit");
        System.out.println("Choice:");
        handleMainMenuInput(readInput());

    }

    public void handleMainMenuInput(int choice){
        switch (choice) {
            case 1:
                System.out.println("Enter a name for the event:");
                String name = readTextInput();
                System.out.println("Enter a date for the event(dd-mm-yyyy):");
                String date = readTextInput();
                while(!calendar.isValidDate(date)){
                    System.out.println("Enter a date for the event(dd-mm-yyyy):");
                    date = readTextInput();}
                calendar.addEvent(name,date);
                mainMenu();
                break;
            case 2:
                calendar.show();
                mainMenu();
                break;
            case 3:
                System.exit(0);
                break;
            default:
                System.out.println("\nInvalid input!");
                mainMenu();
                break;


        }
    }

    public Integer readInput() {
        int choice = 0;
        try {
            Scanner scanner = new Scanner(System.in);
            choice = scanner.nextInt();
        } catch (InputMismatchException e){
            mainMenu();
        }
        return choice;
    }

    public String readTextInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
